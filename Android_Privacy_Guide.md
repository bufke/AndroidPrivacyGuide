﻿In this list I will share **privacy respecting** apps/services to replace big ones that will harm your privacy. All suggestions here doesn't require root access if don't specifically mentioned. *Please share your apps/thoughts and help to improve this list* 👍
﻿
Apps/services must meet these requirements:

- Open-source because we can't verify what closed source apps are really doing
- Doesn't collect user data or absolutely minimal that can't identify user
- Easy to setup

I'm not affiliated with any app/service and there might be some typo errors because English isn't my native language.


# OS
You shouldn't use Stock ROM that came with your device. Stock ROMs includes **proprietary** (like Google Apps such as the Play Store and [Google Play Services](https://en.wikipedia.org/wiki/Google_Play_Services) ) **apps** that will **spy you**.  Google even tracks your location when it's turned [off](https://www.techdirt.com/articles/20171121/09030238658/investigation-finds-google-collected-location-data-even-with-location-services-turned-off.shtml).

*Operating Systems* that are listed below will not spy you.

[LineageOS](https://www.lineageos.org/)
> Open source operating system that respects your freedom

[RattleSnakeOS](https://github.com/dan-v/rattlesnakeos-stack)
>Successor of popular CopperheadOS with advanced security features.

>NOTE: You need to build this by yourself

[microG](https://microg.org)
>Open-source alternative for Google Play Services.

>You can also check [LineageOS for microG](https://lineage.microg.org)

>Note that **microG isn't operating system**

[Magisk](https://forum.xda-developers.com/apps/magisk)
> Most popular and open-source root method.

> WARNING: **DO NOT** use SuperSU. **It's closed source and SuperSU is owned by Chinese company**


# App stores
Do not use Google Play Store. It collects information about your installed apps and Google even have ability to [**remove and install apps without your permissions**](https://jon.oberheide.org/blog/2010/06/25/remote-kill-and-install-on-google-android/)

*App stores* that are listed below are open-source and respects your freedom.

[F-Droid](https://f-droid.org/en/)
>Community-maintained software repository of FOSS (*Free and Open Source Software*) apps.

>You can also add your own respositories

[YalpStore](https://github.com/yeriomin/YalpStore)
>YalpStore allows you to download apps from Google Play Store without violating your privacy.

>You can also check [AuroraStore](https://gitlab.com/AuroraOSS/AuroraStore) which is fork of YalpStore with material design enabled


# Browsers
[Do not use Google Chrome](https://spyware.neocities.org/articles/chrome.html). It's huge spyware and **will track everything you do online**. 

*Browsers* that are listed below will not track you and are fully open-source.

[Bromite](https://www.bromite.org/)
> Chromium based browser with built-in ad blocking and privacy enhancements


[Fennec F-Droid](https://f-droid.org/en/packages/org.mozilla.fennec_fdroid/)
> Firefox based browser which have removed proprietary bits that are found in official Firefox.


# Search Engines

[Do not use Google search](https://spyware.neocities.org/articles/google.html). **It will build profile from your searches** and knows your location. 

*Search Engines* that are listed below will not build profile about you.

[StartPage](https://www.startpage.com/)

>Uses Google search to provide results. Google will only see StartPage, it will not see you

[DuckDuckGo](https://duckduckgo.com/)
> DuckDuckGo doesn't save your searches and your location. Provides Yahoo and Bing results.

> WARNING: Based on [US](https://www.privacytools.io/#ukusa) and hosted on Amazon servers

[Searx](https://searx.me/)
> Provides search results from multiple search engines, including Google search. Run by individual.

> If you don't trust individual persons then use [Searx by Disroot](https://search.disroot.org/)

# Messengers
Do not use closed source messengers like WhatsApp, Telegram, Hangouts or Threema. We can't verify what they are really doing in background. 

*Messengers* that are listed below are open-source and are encrypted so no one can read your messages.

[Wire](https://wire.com/en/)

> Switzerland based company and doesn't collect information about users. Can also make calls

[Conversations] (https://conversations.im) - [Free on F-Droid] (https://f-droid.org/en/packages/eu.siacs.conversations/)
> Open-source client for Jabber/XMPP protocols. Can't make calls.

> You can host your own XMPP server or select from trusted providers. Good lists of providers are [Official list from Conversations] (https://conversations.im/compliance/) or [Public XMPP servers] (https://list.jabber.at) list.


Some trusted providers:
> [XMPP.is] (https://xmpp.is/)

>Based on Germany and doesn't collect users messages or IP addresses.

[Briar](https://briarproject.org/index.html)
> Doesn't rely on a central server and works without Internet (through Bluetooth or Wi-FI). Also hides metadata. Can't make calls. 

> WARNING: I'm not sure how reliable this messenger is and how many bugs it's have

[Signal](https://signal.org/)
> Encrypted messenger & calling app. Doesn't collect information about users.

> WARNING: Signal is based on [US](https://www.privacytools.io/#ukusa) and [doesn't work properly without Google Cloud Messaging](https://github.com/signalapp/libsignal-service-java/pull/53#issuecomment-404507178)

[Silence](https://silence.im/)
> Silence is a full replacement for the default text messaging app. Encrypts your communications between other Silence users.
> WARNING: For non-Silence users communications aren't encrypted.

# Email providers

Do not use Gmail. It's owned by Google which **will scan all your emails**. 

*Email providers* that are listed below will not read your emails

[Tutanota](https://tutanota.com/)
> Open-source encrypted email provider located in Germany. Also encrypts metadata. Free plan with 1GB storage. 

>Tutanota offers [beta app](https://tutanota.uservoice.com/knowledgebase/articles/483300-where-can-i-get-the-tutanota-app) that works without Google Play Services. [They are planning to publich app on F-Droid too](https://tutanota.com/blog/posts/secure-mail-open-source).

>Also have [paid plans](https://tutanota.com/pricing)

[Posteo](https://posteo.de/en)
> Email provider that doesn't collect your personal information. Based on Germany. *1€/month*

> WARNING: Encryption isn't on by default.

[ProtonMail](https://protonmail.com/)
> Encrypted email provider based on Switzerland.

> WARNING: Doesn't encrypt metadata.


[K-9 Mail](https://k9mail.github.io)
>K-9 Mail is open-source email client


# VPN providers
Getting good VPN is important on Android.

All *VPN providers* that are listed below have **no logging policy** and allows torrenting
.  
> You can never trust fully trust VPN service. **There have been cases that the VPN service has claimed not to collect logs but has still logged everything**. 

>*You are just moving your trust from your ISP to VPN provider*. If you need real anonymity use [TOR](https://www.torproject.org) or [Tails](https://tails.boum.org)

[Mullvad](https://mullvad.net/en/)
> No log policy and based on Sweden. €5/month and supports Bitcoin

[SigaVPN](https://sigavpn.org/)
> No log policy and based on US. Doesn't cost anything but you can get extra services for donating.

> WARNING: Service is based on [US](https://www.privacytools.io/#ukusa)

>Also it's free but developer have said that it [doesn't log anything](https://sigavpn.org/privacy.html) and is run by donations and users that mines for service (optional). 

>*Use with caution*!

VPN Services to avoid:
>These services are known to be harmful for your privacy and shouldn't never be used:

>IPVanish, Hotspot Shield, PureVPN, Private Internet Access (**Check mention below about PIA**) and generally any free VPN service (**Check above mentioned about SigaVPN**)


>*PIA* **is not harmful** for your privacy but **it does have some bad points to consider**. First of all, *PIA* recently [hired *Mark Karpeles* as CTO](https://www.reddit.com/r/PrivateInternetAccess/comments/8eejmr/what_the_fuck_is_pia_thinking_hiring_mark/). 

>And second thing is that *PIA* is based on [US](https://www.privacytools.io/#ukusa). Yes, I know that *SigaVPN is based on US too* but **when you pay for the VPN service you need to get best privacy as possible** and with *PIA you won't get it*.


# Cloud services 
Do not use Drive for cloud storage solutions. It's owned by Google and it **will read all your files**.
Google Photos is not recommed solution too because Google will scan all your photos and save them.

All *cloud services* that are listed below will not access your files and are open-source.
**Always encrypt your files before uploading them to cloud**.

[Nextcloud] (https://nextcloud.com/)
> You can host your own Nextcloud or use on of the trusted providers below.

Nextcloud Providers:

>[Woelkli] (https://woelkli.com/en) FREE/PRO

>[Disroot] (https://disroot.org/en/services/nextcloud) 4GB/FREE


# Password managers
Creating strong passwords are important part of privacy & security so that you account can't be compromised.

[Edward Snowden on passwords](https://www.invidio.us/watch?v=yzGzB-yYKcc)

All *password managers* listed below are open-source

[KeePass DX](https://github.com/Kunzisoft/KeePassDX)
> Fork of popular KeePass for Android - Offline only

[Bitwarden] (https://bitwarden.com/)
> Open-source password manager that can sync your passwords on all your devices


# Note-taking apps
You shouldn't use Google Keep because Google have access all your private notes.

*Note-taking apps* that are listed below wont access your notes.

[Standard notes] (https://standardnotes.org/)
> Encrypted note-taking app that can sync your notes on all your devices

[Joplin](https://joplin.cozic.net/)
> Open source and encrypted note taking and to-do application. Can sync between devices. 

> Good replacement for Evernote.

[Simple Notes] (https://github.com/SimpleMobileTools/Simple-Notes)
> Local note-taking app. Doesn't have encryption.


# Ad blocking on Android

[Blokada](https://blokada.org)
>Open-source ad blocker. Use [AdAway](https://adaway.org) if you have *rooted device*. 

>User can also change DNS address on Blokada.

[NetGuard](https://www.netguard.me)
>Block apps from accessing Internet. Use [AFWall+](https://github.com/ukanth/afwall) if you have *rooted device*. 

>User can also change DNS addresses on both apps.


Privacy respecting DNS servers
>[UncensoredDNS](https://blog.uncensoreddns.org)

>Based on Denmark and doesn't log users (*only logs traffic volume*).

>[SecureDNS](https://securedns.eu)

>Based on Netherlands and doesn't log users.


**DO NOT** use [Cloudfare's DNS](https://www.reddit.com/r/sevengali/comments/8fy15e/) servers. Cloudfare isn't [privacy respecting](https://www.reddit.com/r/privacy/comments/41cb4k/be_careful_with_cloudflare/) company. 


# Miscellaneous apps

YouTube
> App: [NewPipe](https://newpipe.schabi.org) allows you to watch YouTube videos without violating your privacy.

> App: [SkyTube](https://github.com/ram-on/SkyTube) allows you to watch YouTube videos without violating your privacy. Includes some "extra features" like reading comments.

> Website: [Invidio](https://www.invidio.us) allows you to watch YouTube videos without violating your privacy

Gboard
>[AnySoftKeyboard](https://anysoftkeyboard.github.io) doesn't collect information about users

>AOSP keyboard that comes pre-installed with many Custom ROMs (like on LineageOS)


[OsmAnd](https://osmand.net)
> Maps & Navigation app that respects your privacy

[Slide for Reddit](https://github.com/ccrama/Slide)
> Open-source, ad free Reddit browser

[andOTP](https://github.com/andOTP/andOTP)
> Use this app to control your two-factor authentication codes.

> Fork of OTP Authenticator.


**DO NOT USE FACEBOOK**
> Facebook is known for *listening your microphone without your permission* (**there isn't any official proof, only many reports**) *and gathering all your data*

> Two well explained Reddit posts about Facebook listening: [Post 1](https://www.reddit.com/r/privacy/comments/7mxn9i/is_facebook_listening_and_creating/) and [Post 2](https://www.reddit.com/r/privacy/comments/7n4b8g/part_2_of_facebook_listening_test_listening_to_a/)
